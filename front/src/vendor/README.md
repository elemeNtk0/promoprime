# Сторонний код #

В данном каталоге находятся папки со сторонними программными решениями - jQuery, Bootstrap, слайдеры и прочее. Их не следует сжимать вместе со своими стилями и скриптами потому что:

- Они требуются не на каждой странице.
- Проще разобраться, какой сторонний код был использован в проекте.

## Использование ##

Рассмотрим на примере jQuery.

- Создаем подкаталог ./jquery.
- Загружаем в него файл jquery-2.0.1.min.js
- Подключаем его в html странице с помощью конструкции:

```html
<script src="vendor/jquery/jquery-2.0.1.min.js"></script>
```
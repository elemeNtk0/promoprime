// //
// Главный файл Js - точка сборки.
// В этом файле следует подключать другие Js файлы.
// //
$(document).ready(function() {

  $('#js-stickyPanel').length && stickyPanel.init();
  $('#js-mainCatalog').length && mainCatalog.init();
  $('#js-searchPanel').length && searchPanel.init();
  $('.cd-top').length && cdTop.init();
  $('.js-tabs').length && tabs.init();
  // $('#js-rangeSliderArea').length && customRangeSlider.init();
  // $('.js-slick-slider').length && simpleGallery.init();



  $('.js-magnificPopup').magnificPopup({
    type:'inline',
      removalDelay: 500,
      callbacks: {
        beforeOpen: function() {
          this.st.mainClass = this.st.el.attr('data-effect');
          var dataItemThumb = this.st.el.attr('data-item-thumb');
          var dataItemName = this.st.el.attr('data-item-name');
          var popupContainer = $('#addCartPopup');
          popupContainer.find('.popup__cart-img-wrap > img').attr("src", dataItemThumb);
          popupContainer.find('.popup__cart-item-name').text(dataItemName);
        }
      },
    midClick: true,
  });




  $("#js-sliderBigMainPage").lightSlider({
    addClass: 'lSSlideOuter--bann-big',
    enableDrag: false,
    item: 1,
    loop: false,
    controls: false,
    slideMove: 1,
    slideMargin: 0,
    easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
    speed: 600,
  });

  $('#js-sliderCardProduct').lightSlider({
    gallery: true,
    item: 1,
    vertical: true,
    verticalHeight: 320,
    vThumbWidth: 68,
    thumbItem: 4,
    thumbMargin: 10,
    slideMargin: 0,
  });

  $("#js-sliderDiscounts").lightSlider({
    // addClass: 'lSSlideOuter--bann-big',
    enableDrag: false,
    item: 1,
    loop: true,
    pager: false,
    controls: false,
    pauseOnHover: true,
    auto: true,
    pause: 3000,
    slideMove: 1,
    slideMargin: 0,
    easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
    speed: 600,
  });

  $(".js-zoomImg").imagezoomsl({
    zoomrange: [3, 3]
  });


  $('#js-video-gallery').lightGallery({
    oadYoutubeThumbnail: true,
    youtubeThumbSize: 'default',
    loadVimeoThumbnail: true,
    vimeoThumbSize: 'thumbnail_medium',
    // controls: false,
    autoplay: false,
    download: false,
    autoplayControls: false,
    fullScreen: false,
    zoom: false,
    selector: '.slider-scrolling__link',
  });

  $(".mCustomScrollbar").mCustomScrollbar({
    axis:"x",
    advanced: {
      updateOnContentResize : true,
      autoExpandHorizontalScroll: true
    },
    theme: "inset-dark",
  });

  $("#js-contactsForm").validate({
    focusCleanup: true,
    focusInvalid: false,
    errorClass: "validate__message",
    errorElement: "div",
    validClass: "success",
    highlight: function(element) {
      $(element).addClass('form__control--invalid').removeClass('form__control--valid');
    },
    unhighlight: function(element) {
      $(element).removeClass('form__control--invalid').addClass('form__control--valid');
    }
  });
  $("#js-callBackForm").validate({
    focusCleanup: true,
    focusInvalid: false,
    errorClass: "validate__message",
    errorElement: "div",
    validClass: "success",
    highlight: function(element) {
      $(element).addClass('form__control--invalid').removeClass('form__control--valid');
    },
    unhighlight: function(element) {
      $(element).removeClass('form__control--invalid').addClass('form__control--valid');
    }
  });


  // text-preview
  if ($('.js-text-preview').length) {
    var closedHeight = 190;

    $.each($('.js-text-preview'), function() {
      var previewHeight = + $(this).removeClass('closed').outerHeight(true);
      if (previewHeight > closedHeight) {
        $(this).css('height', previewHeight+'px').addClass('closed');
      } else {
        $(this).find('.text-preview__control').hide();
      }
    });

    $(document).on('click','.text-preview__control', function (e) {
      e.preventDefault();
      $(this).closest('.js-text-preview').toggleClass('closed');
    });
  }

  if($('.js-lightFilter').length) {
    var lightFilter = $('.js-lightFilter');
    var lightFilterItems = lightFilter.find('a.light-filter__item');

    lightFilterItems.on('click', function(e){
      e.preventDefault();
      var $this = $(this);
      $this.toggleClass('light-filter__item--active');
    });
  }

  // collapsible left menu
  $('#js-leftMenu').length && CollapsibleLists.applyTo(document.getElementById('js-leftMenu'));

  $.protip();


}); // END doc.ready

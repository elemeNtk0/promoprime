if ($('#js-rangeSliderArea') && $('#filterCostMin') && $('#filterCostMax')) {


 var
    $range = $("#js-rangeSliderArea"),
    $inputFrom = $('#filterCostMin'),
    $inputTo = $('#filterCostMax'),
    changing = false;

    var OnChangeSlider = function(data) {
      var from = data.from;
      var to = data.to;
      $inputFrom.val(from);
      $inputTo.val(to);
    };

    $range.ionRangeSlider({
      type: "double",
      min: 0,
      max: 10000,
      from: $inputFrom.val(),
      to: $inputTo.val(),
      force_edges: true,
      grid: false,
      // grid_num: 20,
      hide_min_max: true,
      onStart: function(data) {
        var toValue = $inputTo.val() * 1,
        fromValue = $inputFrom.val() * 1;
        if (toValue == fromValue && (toValue === null || toValue === 0 || toValue === '')) {
          data.to = data.max;
          data.from = data.min;
        }
      },
      onChange: OnChangeSlider,
      onFinish: OnChangeSlider,
      onUpdate: function(data) {
        if (changing) {
          changing = false;
          return;
        };
        OnChangeSlider(data);
      }
    });

    var sliderData = $range.data("ionRangeSlider");

    $inputFrom.change(function() {
      var value = $inputFrom.val() * 1,
        toValue = $inputTo.val() * 1,
        rangeMax = sliderData.result.max * 1;

      if (value >= rangeMax && ( toValue === '')) {
        value = rangeMax;
        $inputTo.val('');
      }

      else if (toValue === '' || toValue === 0) {
        value = value;
        $inputTo.val('');
      }
      else if (value >= toValue && toValue !== '') value = toValue;
      if (value !== '' && value !== null)
        $inputFrom.val(value);

      changing = true;
      var inputVal = $inputFrom.val() * 1;

      sliderData.update({
        from: inputVal
      });
    });

    $inputTo.change(function() {
      var value = $inputTo.val() * 1,
        fromValue = $inputFrom.val() * 1,
        rangeMax = sliderData.result.max * 1;

      if (value < rangeMax) {
        if (value < fromValue && value !== '') value = fromValue;
      }
      /* Если тут впилить else if, то работает криво ((( */
      if (value >= rangeMax) value = rangeMax;
      else if (value === 0 || value === '' || value === null)
        value = '';

      $inputTo.val(value);

      changing = true;
      var inputVal = $inputTo.val() * 1;
      if (value !== '')
        sliderData.update({to: inputVal});
      else sliderData.update({to: rangeMax});
    }); // end range-slider

}

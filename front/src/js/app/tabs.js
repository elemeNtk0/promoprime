var tabs = (function() {
  var me = {};

  // .js-tabs / .js-tabs-search
  // .js-tabsNavItem
  // .js-tabsNavLink
  // .js-tabsItem
  me.init = function() {
    $(document).on('click','.js-tabsNavLink',onTabClick);
    onPageReady();
  };

  // scan tabs on page loaded. If no active tab-nav found, select first tab-nav and its tab
  function onPageReady() {
    $('.js-tabs').each(function(){
      var $self = $(this);

      // if (! ($self.hasClass('.js-tabs-search') || $self.find('.js-tabsNavItem.active').length)) {
      //   $self.find('.js-tabsNavItem .js-tabsNavLink').first().trigger('click');
      // }
    });
  }




  function onTabClick(e) {
    e = e || window.event;
    var $self = $(e.target),
        $tabsNav = $self.closest('.js-tabsNavItem'),
        $tabs = $self.closest('.js-tabs');

    e.preventDefault();

    if (! $tabsNav.hasClass('active')) {
      // you can provide additional selector via data-toggle attribute,
      // if you want to show/hide more than one element per click
      var activeTab = $self.data('toggle') ? $self.data('toggle') : $self.attr('href');

      if ($(activeTab).length) {

        $tabs.trigger('tabs.beforeChange');

        $tabs.find('.js-tabsItem.active').filter(function(){
          return $(this).closest('.js-tabs').is($tabs);
        }).removeClass('active');
        $tabs.find('.js-tabsNavItem.active').filter(function(){
          return $(this).closest('.js-tabs').is($tabs);
        }).removeClass('active');

        $(activeTab).filter('.js-tabsItem').addClass('active');
        $tabsNav.addClass('active');

        $tabs.trigger('tabs.afterChange');
      }
    } else if ($tabs.hasClass('js-tabs-search')) {
      $tabsNav.removeClass('active');
      $tabs.find('.js-tabsItem.active').filter(function(){
        return $(this).closest('.js-tabs').is($tabs);
      }).removeClass('active');
    }
  }

  me.tabClick = onTabClick;

  return me;
}());

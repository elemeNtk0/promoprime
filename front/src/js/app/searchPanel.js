var searchPanel = (function() {
  var me = {},
      $searchPanel,
      $btnSearch,
      $btnSearchPanelClose;

  me.init = function() {
    $searchPanel = $('#js-searchPanel');
    $btnSearch = $('#js-btnSearch');
    $btnSearchPanelClose = $('#js-btnSearchPanelClose');
    $btnSearch.on('click', btnSearchClick);
    $btnSearchPanelClose.on('click', searchPanelHide);
  };

  function btnSearchClick() {
    var searchPanelIsHidden = $searchPanel.hasClass('-hidden');

    if (searchPanelIsHidden)
      $searchPanel.removeClass('-hidden');
    else
      $searchPanel.addClass('-hidden');
  }

  function searchPanelHide() {
    // var searchPanelIsHidden = $searchPanel.hasClass('-hidden');
    $searchPanel.addClass('-hidden');
  }
  return me;
}());

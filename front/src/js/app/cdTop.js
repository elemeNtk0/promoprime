var cdTop = (function() {
  var me = {},
      offset,
      offset_opacity,
      scroll_top_duration,
      $back_to_top;

  me.init = function() {
    offset = 300;
    offset_opacity = 1200;
    scroll_top_duration = 700;
    $back_to_top = $('.cd-top');
    $(window).on('scroll', onScrollCdTop);
    $back_to_top.on('click', topClick);
  };


  function onScrollCdTop() {
    ( $(this).scrollTop() > offset ) ? $back_to_top.addClass('cd-is-visible') : $back_to_top.removeClass('cd-is-visible cd-fade-out');
    if( $(this).scrollTop() > offset_opacity ) {
      $back_to_top.addClass('cd-fade-out');
    }
  };

  function topClick(e) {
    e.preventDefault();
    $('body,html').animate({
      scrollTop: 0 , }, scroll_top_duration );
  }



  return me;
}());
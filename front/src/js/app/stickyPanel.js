var stickyPanel = (function() {
  var me = {},
      $stickyPanel,
      $btnHamburger,
      $btnSearch,
      $headerCatalog,
      $mainCatalog,
      $searchPanel,
      stickyPanelOffset;

  me.init = function() {
    $stickyPanel = $('#js-stickyPanel');
    $btnHamburger = $('#js-btnHamburger');
    $btnSearch = $('#js-btnSearch');
    $headerCatalog = $('#js-mainCatalog.header__catalog');
    $mainCatalog = $('#js-mainCatalog');
    $searchPanel = $('#js-searchPanel');
    stickyPanelOffset = $stickyPanel.length ? $stickyPanel.offset().top : 125;
    $(window).on('scroll', onScroll);
  };

  function onScroll() {
    var currentScroll = $(this).scrollTop();

    if (currentScroll < stickyPanelOffset) {
      $stickyPanel.removeClass('header-panel--sticky');
      $btnHamburger.removeClass('btn--hamburger-sticky');
      $btnSearch.removeClass('btn--search-sticky');
      $searchPanel.removeClass('search-panel--sticky');
      if ($headerCatalog.length > 0)  {
        $headerCatalog.removeClass('header__catalog--sticky');
        return;
      }
      $mainCatalog.removeClass('main-navigation__catalog-menu--moved');

    }
    else {
      $stickyPanel.addClass('header-panel--sticky');
      $btnHamburger.addClass('btn--hamburger-sticky');
      $btnSearch.addClass('btn--search-sticky');
      $searchPanel.addClass('search-panel--sticky');
      if ($headerCatalog.length > 0)  {
        $headerCatalog.addClass('header__catalog--sticky');
        return;
      }
      $mainCatalog.addClass('main-navigation__catalog-menu--moved');
    }
  }
  return me;
}());

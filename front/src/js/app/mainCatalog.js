var mainCatalog = (function() {
  var me = {},
      $mainCatalog,
      $btnHamburger;

  me.init = function() {
    $mainCatalog = $('#js-mainCatalog');
    $btnHamburger = $('#js-btnHamburger');
    $btnHamburger.removeClass('btn--hamburger-active');
    $btnHamburger.on('click', hamburgerClick);
  };

  function hamburgerClick() {
    var hamburgerIsActive = $btnHamburger.hasClass('btn--hamburger-active');

    if (hamburgerIsActive)
      $btnHamburger.removeClass('btn--hamburger-active');
    else
      $btnHamburger.addClass('btn--hamburger-active');

    mainCatalogShowing();
  }

  function mainCatalogShowing() {
    var mainCatalogIsHidden = $mainCatalog.hasClass('-hidden');

    if (mainCatalogIsHidden)
      $mainCatalog.removeClass('-hidden');
    else
      $mainCatalog.addClass('-hidden');

  }
  return me;
}());
